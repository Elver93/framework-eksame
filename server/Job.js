module.exports = (mongoose) => {
    const express = require('express');
    const router = express.Router();

    //skema
    let categorySchema = mongoose.Schema({
        id: Number,
        category: String,
        url: String
    });

    let categoryModel = mongoose.model('category', categorySchema, "categories");


    //skema
    let areaSchema = mongoose.Schema({
        id: Number,
        area: String,
        url: String
    });

    let areaModel = mongoose.model('area', areaSchema, "areas");


    //skema
    let jobSchema = mongoose.Schema({
        id: Number,
        title: String,
        description: String,
        date: String,
        category: String,
        area: String,
        categoryid: Number,
        areaid: Number,
        username: String
    });

    let jobModel = mongoose.model('job', jobSchema);

    //rute til at hente
    router.get('/cat', (req,res) => {
        categoryModel.find((err, cat) => {
            if (err) return console.log(err);
            res.json(cat);
        });
    });


    //rute til at hente
    router.get('/area', (req,res) => {
        areaModel.find((err, area) => {
            if (err) return console.log(err);
            res.json(area);
        });
    });


    //rute til at hente en bruger
    router.get('/:cat/:area', (req,res) => {
        jobModel.find({category: req.params.cat, area: req.params.area}, (err, job) => {
            if (err) return console.log(err);
            res.json(job);
        });
    });

    router.get('/:id', (req,res) => {
        jobModel.find({id: req.params.id}, (err, job) => {
            if (err) return console.log(err);
            res.json(job);
        });
    });

    router.get('/', (req,res) => {
        jobModel.find({}).sort({id: -1}).limit(6).exec((err, job) => {
            if (err) return console.log(err);
            res.json(job);
        });
    });

    router.post('/', (req,res) => {
        let tmp = new jobModel();
        let date = new Date();

        jobModel.find({}).sort({id: -1}).limit(1).exec((err, jobs) =>{
            let id = 1;
            if (jobs.length > 0)
            {
                id = jobs[0].id;
                ++id;
            }
            tmp.id = id;
            tmp.title = req.body.title;
            tmp.description = req.body.description;
            tmp.date = date.getFullYear() + "-" + ("0" +(date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
            tmp.category = req.body.category;
            tmp.area = req.body.area;
            tmp.categoryid = req.body.categoryid;
            tmp.areaid = req.body.areaid;
            tmp.username = req.user.username;

            tmp.save();

            res.json({msg: "you have created at job post", id: id})
        });
    });

    return router;
}