const compression = require('compression')
const path = require('path');
const express = require('express');
const bodyparser = require('body-parser');
const logger = require("morgan");
const app = express();
const port = (process.env.PORT || 3001);
const mongoose = require('mongoose');
const checkJwt = require('express-jwt');
const pathToRegexp = require('path-to-regexp');
//mongoose.connect('mongodb://dbuser:niels2502@192.168.1.142/framework_mikkel?authSource=admin', {useNewUrlParser: true});
mongoose.connect('mongodb://dbuser:niels2502@5.186.60.140/framework_mikkel?authSource=admin', {useNewUrlParser: true});

if (!process.env.JWT_SECRET) {
    console.error('You need to put a secret in the JWT_SECRET env variable!');
    process.exit(1);
}

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected to Mongodb")
});

app.use(compression())
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());

app.use(logger("dev"));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

    // intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        // respond with 200
        console.log("Allowing OPTIONS");
        res.sendStatus(200);
    } else {
        // move on
        next();
    }
});

//server static filer fra react
app.use(express.static(path.join(__dirname, '../build')));

let openPath = function(req) {
    if (req.path === '/api/users/login')return true;
    else if(req.path === '/api/users' && req.method === "POST") return true;
    else if(req.method === "GET")
    {
        if(req.path === '/api/jobs') return true;
        else if(req.path === '/api/jobs/cat') return true;
        else if(req.path === '/api/jobs/area') return true;
        else if(pathToRegexp('/api/jobs/:id').test(req.path)) return true;
        else if(pathToRegexp('/api/jobs/:id/:id').test(req.path)) return true;
        else if(req.path === '/home') return true;
        else if(req.path === '/category') return true;
        else if(req.path === '/login') return true;
        else if(req.path === '/profile') return true;
        else if(req.path === '/addjob') return true;
        else if(pathToRegexp('/area/:id').test(req.path)) return true;
        else if(pathToRegexp('/jobs/:id/:id').test(req.path)) return true;
        else if(pathToRegexp('/job/:id').test(req.path)) return true;

    }
    return false;
}

// Validate the user using authentication
app.use(
    checkJwt({ secret: process.env.JWT_SECRET }).unless(openPath)
);

app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({ error: err.message });
    }
});





app.use((err, req, res, next) =>{
    console.error(err.stack);
    res.status(500).send({msg: 'Something Broke'})
});

let usersRouter = require('./User')(mongoose);
let jobsRouter = require('./Job')(mongoose);
app.use('/api/users', usersRouter);
app.use('/api/jobs', jobsRouter);


/**** Reroute all unknown requests to the React index.html ****/
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.listen(port, () => console.log(`Api running on port ${port}!`));