import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';

export default class Area extends Component {
    constructor(props) {
        super(props);

        this.props.getArea();
    }

    render(){
        let tmparea = [];

        if(this.props.areas){
            this.props.areas.forEach(areas => {
                tmparea.push(<NavLink key={areas.id} to={"/jobs/" + this.props.catid + "/" + areas.area.replace(/ /g, '-')}>{areas.area}</NavLink>)
            })
        }

        return (
            <div className="content">
                <h1>Pick a area</h1>
                <div className={"cats"}>
                    {tmparea}
                </div>
            </div>
        );
    }
}