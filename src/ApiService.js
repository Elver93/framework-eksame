var jwtDecode = require('jwt-decode');
/**
 * Service class for authenticating users against an API
 * and storing JSON Web Tokens in the browsers LocalStorage.
 */
class ApiService {

    constructor(api_users, api_jobs) {
        this.api_users = api_users;
        this.api_jobs = api_jobs;
        this.fetch = this.fetch.bind(this);
        this.login = this.login.bind(this);
    }

    addUser(json) {
        return this.fetch(this.api_users, {
            method: 'POST',
            body: JSON.stringify(json)
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    login(username, password) {
        return this.fetch(this.api_users + "/login", {
            method: 'POST',
            body: JSON.stringify({
                username,
                password
            })
        }).then(res => {
            if (res.token !== undefined)
            this.setToken(res.token);
            return Promise.resolve(res);
        })
    }

    getUserInfo()
    {
        let token = this.getToken();
        if (token) return jwtDecode(this.getToken());

        return null;
    }

    loggedIn() {
        if (this.getToken() !== null && this.getToken() !== undefined)
        {
            if (jwtDecode(this.getToken()).exp < Date.now() / 1000) {
                this.logout();
                console.log('token expired');
            }
        }

        return (this.getToken() !== undefined && this.getToken() != null);
    }

    setToken(token) {
        localStorage.setItem('token', token)
    }

    getToken() {
        return localStorage.getItem('token')
    }

    logout() {
        localStorage.removeItem('token');
    }

    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
        .then(response => response.json());
    }

    getArea() {
        return this.fetch(this.api_jobs + "/area", {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    getCat() {
        return this.fetch(this.api_jobs + "/cat", {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    add(json) {
        return this.fetch(this.api_jobs, {
            method: 'POST',
            body: JSON.stringify(json)
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    getJobs(cat,area) {
        return this.fetch(this.api_jobs + "/" + cat + "/" + area, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    getJob(id) {
        return this.fetch(this.api_jobs + "/" + id, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    getHomeJobs() {
        return this.fetch(this.api_jobs, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res);
        })
    }
}

export default ApiService;
