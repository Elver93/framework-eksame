import '@testing-library/react/cleanup-after-each'
import "jest-dom/extend-expect"

global.cat = [{category: "Test cat 1", id: 1},{category: "Test cat 3", id: 3},{category: "Test cat 2", id: 2}]

global.area = [{area: "Test area 1", id: 1},{area: "Test area 3", id: 3},{area: "Test area 2", id: 2},{area: "Test area 5", id: 5 },{area: "Test area 4", id: 4}]
