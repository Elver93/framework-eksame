import React, {Component} from 'react';
import { NavLink} from 'react-router-dom';

export default class Jobs extends Component {
    constructor(props) {
        super(props);

        this.props.getJobs(this.props.catid.replace(/-/g, ' '), this.props.areaid.replace(/-/g, ' '));
    }

    render(){
        let tmpjobs = [];
        let lbl = "";

        if(this.props.jobs.length > 0){
            this.props.jobs.forEach((job, index) =>{
                tmpjobs.push(<div key={index} ><NavLink to={"/job/" + job.id}><h2>{job.title}</h2></NavLink><p>{job.description}</p></div>)
            })
        }
        else{
            lbl = "Their is no jobs for this search try another area";
        }

        return (
            <div className="content">
                <h1>{this.props.catid.replace(/-/g, ' ') + " - " + this.props.areaid.replace(/-/g, ' ')}</h1>
                <div className={"jobs"}>
                    {tmpjobs}
                </div>
                {lbl}
            </div>
        );
    }
}