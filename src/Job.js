import React, {Component} from 'react';

export default class Job extends Component {
    constructor(props) {
        super(props);

        this.props.getJob(this.props.id)
    }

    render(){
        let tmpjob = "";

        if(this.props.job){
            tmpjob = <>
                <div className={"row"}>
                    <div className={"col-2"}>
                        <h3>Added date</h3>
                        <p>{this.props.job[0].date}</p>
                        <h3>Category</h3>
                        <p>{this.props.job[0].category}</p>
                        <h3>Area</h3>
                        <p>{this.props.job[0].area}</p>
                        <h3>Posted by</h3>
                        <p>{this.props.job[0].username}</p>
                    </div>
                    <div className={"col-10"}>
                        <h1>{this.props.job[0].title}</h1>
                        <p>{this.props.job[0].description}</p>
                    </div>
                </div>
            </>;
        }

        return (
            <div className="content">
                <div className={"job"}>
                    {tmpjob}
                </div>
            </div>
        );
    }
}