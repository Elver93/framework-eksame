import React, {Component} from 'react';

export default class Job extends Component {
    handle = () =>{
        this.props.logout()
        this.props.history.push("/home")
    }

    render(){

        return (
            <button className={"btn btn-success"} onClick={this.handle}>Logout</button>
        );
    }
}