import React, {Component} from 'react';
import { NavLink} from 'react-router-dom';

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.props.getCat();
        this.props.getHomeJobs();
    }

    render(){
        let tmpcat = [];
        let tmpjobs = [];

        if (this.props.categories) {
            this.props.categories.forEach((cat, index) =>{
                tmpcat.push(<NavLink key={index} to={"/area/" + cat.category.replace(/ /g, '-')}>{cat.category}</NavLink>)
            })
        }

        if(this.props.jobs){
            this.props.jobs.forEach((job, index) =>{
                tmpjobs.push(<div key={index}><NavLink  to={"/job/" + job.id}><h2>{job.title}</h2></NavLink><p>{job.description}</p></div>)
            })
        }

        return (
            <div className="content">
                <h1>Categories</h1>
                <div className={"cats"}>
                    {tmpcat}
                </div>
                <h1>New Jobs</h1>
                <div className={"jobs"}>
                    {tmpjobs}
                </div>
            </div>
        );
    }
}