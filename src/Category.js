import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

export default class Category extends Component {
    constructor(props) {
        super(props);

        this.props.getCat();
    }

    render(){
        let tmpcat = [];

        if (this.props.categories) {
            this.props.categories.forEach(cat =>{
                tmpcat.push(<NavLink key={cat.id} to={"/area/" + cat.category.replace(/ /g, '-')}>{cat.category}</NavLink>)
            })
        }

        return (
            <div className="content">
                <h1>Pick a category</h1>
                <div className={"cats"}>
                    {tmpcat}
                </div>
            </div>
        );
    }
}