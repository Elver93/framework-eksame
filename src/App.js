import React, {Component} from 'react';
import { BrowserRouter as Router, NavLink, Link, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';

import Login from './Login';
import ApiService from './ApiService';
import Home from './Home';
import Category from './Category';
import Job from './Job';
import Jobs from './Jobs';
import Area from './Area';
import AddJob from './AddJob';
import Profile from "./Profile";
import Btn from "./Btn";

export default class App extends Component {
    constructor(props) {
        super(props);

        this.Api = new ApiService("/api/users","/api/jobs");
        this.state = {
            categories: [],
            areas: [],
            jobs: [],
            job: "",
            loggedIn:this.Api.loggedIn()
        }
    }

    isLoggedIn =() =>{
        this.setState({loggedIn: !this.state.loggedIn})
    }

    getCat =() =>{
        this.Api.getCat().then(cats => this.setState({categories: cats}))
    }

    getArea =() =>{
        this.Api.getArea().then(areas => this.setState({areas: areas}))
    }

    getJobs =(cat, area) =>{
        this.Api.getJobs(cat, area).then(jobs => this.setState({jobs: jobs}))
    }

    getJob =(id) =>{
        this.Api.getJob(id).then(job => this.setState({job: job}))
    }

    getHomeJobs =() =>{
        this.Api.getHomeJobs().then(jobs => this.setState({jobs: jobs}))
    }

    logout =(e) =>{
        this.Api.logout();
        this.isLoggedIn();
    }

    render(){
    return (
      <Router>
        <div className="wrapper">
          <header>
                <div className="header" >
                    <ul className="left">
                        <li><NavLink activeClassName={"active"} to={"/home"} className="nav-link">Home</NavLink></li>
                        <li><NavLink activeClassName={"active"} to={"/category"} className="nav-link">Find Job</NavLink></li>
                        {this.Api.getUserInfo()&&this.Api.getUserInfo().type===2? <li><NavLink activeClassName={"active"} to={"/addjob"} className="nav-link">Add Job</NavLink></li>:""}
                        {this.Api.loggedIn()===true?<li><NavLink activeClassName={"active"} to={"/profile"} className="nav-link">Profile</NavLink></li>:""}
                    </ul>
                    <ul className="right">
                        {this.Api.loggedIn()===true?<li><p>{this.Api.getUserInfo().username}</p></li>:""}
                        {this.Api.loggedIn()===true?<li><Route path="/" render={(props) => <Btn {...props} logout={this.logout} />} /></li>:""}

                        {this.Api.loggedIn()===false?<li><p>Not logged in</p></li>:""}
                        {this.Api.loggedIn()===false?<li><Link to={"/login/1"} className={"btn btn-success linklogin"}>Login</Link></li>:""}
                    </ul>
                </div>
          </header>
          <section>
              <Switch>
                  <Route exact path="/home" render={(props) => <Home {...props} getCat={this.getCat} categories={this.state.categories} jobs={this.state.jobs} getHomeJobs={this.getHomeJobs} />} />
                  <Route exact path="/category" render={(props) => <Category {...props} getCat={this.getCat} categories={this.state.categories} />} />
                  <Route exact path="/area/:category" render={(props) => <Area {...props} catid={props.match.params.category} getArea={this.getArea} areas={this.state.areas} />} />
                  <Route exact path="/jobs/:category/:area" render={(props) => <Jobs {...props} Api={this.Api} jobs={this.state.jobs} getJobs={this.getJobs} catid={props.match.params.category} areaid={props.match.params.area} />} />
                  <Route exact path="/job/:id" render={(props) => <Job {...props} Api={this.Api} job={this.state.job} getJob={this.getJob} id={props.match.params.id} />} />
                  <Route exact path="/profile" render={(props) => <Profile {...props} Api={this.Api} loggedIn={this.state.loggedIn} />} />
                  <Route exact path="/login/:show" render={(props) => <Login {...props} loggedIn={this.isLoggedIn} show={props.match.params.show} Api={this.Api} />} />
                  <Route exact path="/addjob" render={(props) => <AddJob {...props} Api={this.Api} getArea={this.getArea} areas={this.state.areas} getCat={this.getCat} categories={this.state.categories} />} />
                  <Route path="*" render={(props) => <Redirect to="/home" /> }/>
              </Switch>
          </section>
          <footer>

          </footer>
        </div>
      </Router>
    );
  }
}
