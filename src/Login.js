import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';

export default class Login extends Component {
    handle =(e) =>{
        e.preventDefault();
        this.props.Api.login(this.username.value, this.password.value).then(res => {
            if(res.msg === "User authenticated successfully"){
                this.props.loggedIn();
                this.props.history.push("/home");
            }
        })

    }

    handleReg = (e) =>{
        e.preventDefault()
        let sel = this.type.value;

        if(this.password.value === this.Repassword.value){
            this.props.Api.addUser({
                username: this.username.value,
                password: this.password.value,
                email: this.email.value,
                type: Number(sel)
            }).then(res =>{
                this.msg.innerHTML = res.msg ;

                if(res.msg === "User created"){
                    this.username.value = "";
                    this.password.value = "";
                    this.Repassword.value = "";
                    this.email.value = "";
                    this.type.value = "0";

                    this.msg.innerHTML = res.msg + " you will now be redirected to login";
                    setTimeout(() =>{
                            this.props.history.push("/login/1")
                        },
                        3000);

                }

            })
        }
        else{
            this.msg.innerHTML = "Both passwords need to match";
        }

    }

    render(){

        let show = "";

        if(this.props.show === "2"){
            show = <form onSubmit={this.handleReg}>
                    <label ref={msg => this.msg = msg} />
                    <input ref={username => this.username = username} className="form-control" placeholder={"Username"} />
                    <div />
                    <input type={"password"} ref={password => this.password = password} className="form-control" placeholder={"Password"}/>
                    <div />
                    <input type={"password"} ref={Repassword => this.Repassword = Repassword} className="form-control" placeholder={"Repeat Password"}/>
                    <div />
                    <input ref={email => this.email = email} placeholder={"Email"} className="form-control" />
                    <div />
                    <select ref={type => this.type = type} className="form-control" id="type">
                        <option value={"0"}>-- Pick a account type --</option>
                        <option value={"1"}>Looking for work</option>
                        <option value={"2"}>Firm looking for workers</option>
                    </select>
                    <div />
                    <button type={"submit"} className={"btn btn-success logbtn"}>Register</button>
                </form>

        }else if(this.props.show === "1"){
            show = <form onSubmit={this.handle}>
                    <label ref={msg => this.msg = msg} />
                    <input ref={username => this.username = username} className="form-control" placeholder={"Username"} />
                    <div />
                    <input type={"password"} ref={password => this.password = password} className="form-control" placeholder={"Password"}/>
                    <div />
                    <button type={"submit"} className={"btn btn-success logbtn"}>Login</button>
                    <NavLink to={"/login/2"} className={"logreg"}>Register</NavLink>
                </form>
        }

        return (
            <div className="content">
                <div className={"login"}>
                    {show}
                </div>
            </div>
        );
    }
}
