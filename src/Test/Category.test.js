import React from "react"
import { render } from "@testing-library/react"
import {BrowserRouter as Router } from "react-router-dom";

import Category from '../Category';

it('Test if Category render the category list', () => {
    const {getByText} = render(<Router><Category getCat={jest.fn()} categories={cat} /></Router>);
    expect(getByText("Test cat 1")).toBeInTheDocument();
    expect(getByText("Test cat 2")).toBeInTheDocument();
    expect(getByText("Test cat 3")).toBeInTheDocument();
});

it('test that it renders one h tag and three categories', () => {
    const Cate = render(<Router><Category getCat={jest.fn()} categories={cat} /></Router>);
    expect(document.querySelector('.content').childNodes.length).toBe(4);
});