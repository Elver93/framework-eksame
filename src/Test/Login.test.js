import React from "react"
import { render } from "@testing-library/react"
import {BrowserRouter as Router } from "react-router-dom";

import Login from '../Login';

it('check if login input field contain the right placeholder text', () => {
    const Cate = render(<Router><Login show={"1"} /></Router>);
    expect(document.querySelector('.content form input:nth-of-type(1)').placeholder).toBe("Username");
    expect(document.querySelector('.content form input:nth-of-type(2)').placeholder).toBe("Password");
});