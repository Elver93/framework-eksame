import React from "react"
import { render } from "@testing-library/react"
import {BrowserRouter as Router } from "react-router-dom";

import Area from '../Area';

it('Test if Area render the area list', () => {
    const {getByText} = render(<Router><Area getArea={jest.fn()} areas={area} /></Router>);
    expect(getByText("Test area 3")).toBeInTheDocument();
    expect(getByText("Test area 4")).toBeInTheDocument();
    expect(getByText("Test area 5")).toBeInTheDocument();
});

it('test that it renders one h tag and five areas', () => {
    const tmp = render(<Router><Area getArea={jest.fn()} areas={area} /></Router>);
    expect(document.querySelector('.content').childNodes.length).toBe(6);
});