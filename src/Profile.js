import React, {Component} from 'react';

export default class Profile extends Component {
    constructor(props) {
        super(props);

        if(this.props.loggedIn === false){
            this.props.history.push("/home")
        }
    }

    render(){
        let tmpUserInfo = ""

        if(this.props.Api.getUserInfo() !== null){
            tmpUserInfo = <ul>
                <li><p>Username</p></li>
                <li>{this.props.Api.getUserInfo().username}</li>
                <li><p>Email</p></li>
                <li>{this.props.Api.getUserInfo().email}</li>
                <li><p>Account type</p></li>
                <li>{this.props.Api.getUserInfo().type}</li>
            </ul>
        }

        return (
            <div className="content">
                <h1>Profile</h1>
                <div className={"profile"}>
                    {tmpUserInfo}
                </div>
            </div>
        );
    }
}