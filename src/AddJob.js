import React, {Component} from 'react';

export default class AddJob extends Component {
    constructor(props) {
        super(props);

        this.props.getCat();
        this.props.getArea();

        if(this.props.Api.getUserInfo() && this.props.Api.getUserInfo().type !== 2){
            this.props.history.push("/home")
        }
    }

    handle = (e) =>{
        e.preventDefault()

        let cat = this.cat.value.split("_");
        let area = this.area.value.split("_");

        if(cat[1] === 0) {
            this.err.innerHTML = "Need to pick a category";
        }
        else{
            if(area[1] === 0){
                this.err.innerHTML = "Need to pick a Region";
            }
            else{
                if(this.title.value === "" || this.des.value === ""){
                    this.err.innerHTML = "Need to fill out all the fields";
                }
                else{
                    this.props.Api.add({
                        title: this.title.value,
                        description: this.des.value,
                        category: cat[0],
                        area: area[0],
                        categoryid: Number(cat[1]),
                        areaid: Number(area[1])
                    }).then(res =>{
                        if(res.id){
                            this.props.history.push("/job/"+ res.id)
                        }
                    })
                }
            }
        }

    }


    render(){
        let tmpcat = [];
        let tmparea = [];

        if(this.props.categories){
            this.props.categories.forEach(cat =>{
                tmpcat.push(<option key={cat.id} value={cat.category + "_" + cat.id}>{cat.category}</option>);
            })
        }

        if(this.props.areas){
            this.props.areas.forEach(area =>{
                tmparea.push(<option key={area.id} value={area.area + "_" + area.id}>{area.area}</option>);
            })
        }


        return (
            <div className="content">
                <h1>Add Job</h1>

                <form onSubmit={this.handle}>
                    <div className="form-group">
                        <input ref={title => this.title = title} className="form-control" placeholder={"Job Title"} />
                    </div>
                    <div className="form-group">
                        <textarea ref={des => this.des = des} rows={"4"} className="form-control" placeholder={"Job description"} />
                    </div>
                    <div className="form-group">
                        <select ref={cat => this.cat = cat} className="form-control" id="cat">
                            <option value={"sel_0"}>-- Pick a Category --</option>
                            {tmpcat}
                        </select>
                    </div>

                    <div className="form-group">
                        <select ref={area => this.area = area} className="form-control" id="area">
                            <option value={"sel_0"}>-- Pick a Region --</option>
                            {tmparea}
                        </select>
                    </div>

                    <button type={"submit"} className={"btn btn-success"}>Add job post</button>

                    <label ref={err => this.err = err} />
                </form>
            </div>
        );
    }
}